package view;

import java.util.List;

import model.Employee;

public interface View {
	void showEnrolledEmployees(List<Employee> enrolledEmployees);
}
