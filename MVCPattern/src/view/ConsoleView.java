package view;

import java.util.List;

import model.Employee;

public class ConsoleView implements View {
	@Override
	public void showEnrolledEmployees(List<Employee>enrolledEmployees) {
		System.out.println("This is console view of currently enrolled employees");
		for(Employee employee :enrolledEmployees) {
			System.out.println(employee);
		}
		System.out.println("____________");
	}
}
