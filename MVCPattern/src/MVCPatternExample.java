import controller.Controller;
import controller.EmployeeController;
import model.Employee;
import model.EmployeeModel;
import model.Model;
import view.ConsoleView;
import view.View;

public class MVCPatternExample {
	public static void main(String [] args) {
		Model model =  new EmployeeModel();
		View view = new ConsoleView();
		Controller controller = new EmployeeController(model, view);
		controller.displayEnrolledEmployees();
		controller.addEmployee(new Employee("kevin", "E4"));
		controller.displayEnrolledEmployees();
		controller.removeEmployee("E2");
		controller.displayEnrolledEmployees();
		controller.removeEmployee("E5");
		controller.displayEnrolledEmployees();
		controller.addEmployee(new Employee("kevin", "E4"));
	}

}
