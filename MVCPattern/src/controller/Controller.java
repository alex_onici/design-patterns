package controller;

import model.Employee;

public interface Controller {
	void displayEnrolledEmployees();
	void addEmployee(Employee employee);
	void removeEmployee(String employeeId);

}
