package controller;

import java.util.List;


import model.Employee;
import model.Model;
import view.View;

public class EmployeeController implements Controller {

	private Model model;
	private View view;
	public EmployeeController(Model model, View view) {
		this.model = model;
		this.view = view;
	}
	@Override
	public void displayEnrolledEmployees() {
		List<Employee>enrolledEmployees = model.getEnrolledEmployeeDetailsFromModel();
	     view.showEnrolledEmployees(enrolledEmployees);
	}

	@Override
	public void addEmployee(Employee employee) {
		model.addEmployeeToModel(employee);
	}

	@Override
	public void removeEmployee(String employeeId) {
		model.removeEmployeeFromModel(employeeId);
	}

}
