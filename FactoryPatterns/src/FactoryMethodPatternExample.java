
public class FactoryMethodPatternExample {
	public static void main(String [] args) {
		AnimalFactory tigerFactory = new TigerFactory();
		Animal tigerAnimal = tigerFactory.createAnimal();
		tigerAnimal.speak();
		tigerAnimal.preferredAction();
		System.out.println();
		AnimalFactory dogFactory = new DogFactory();
		Animal dogAnimal = dogFactory.createAnimal();
		dogAnimal.speak();
		dogAnimal.preferredAction();
	}
}
