
public class CompositePatternExample {
	public static void main(String [] args) {
		Employee mathTeacher1 = new Employee("Math Teacher 1", "Maths");
		Employee mathteacher2 = new Employee("Math Teacher 2", "Maths");
		Employee cseTeacher1 = new Employee("CSE Teacher 1", "Computer Sc.");
		Employee cseTeacher2 = new Employee("CSE Teacher 2", "Computer Sc.");
		Employee cseTeacher3 = new Employee("CSE Teacher 3", "Computer Sc.");
		CompositeEmployee hodMaths = new CompositeEmployee("Mrs.S.Das(HOD-Maths", "Maths");
		CompositeEmployee hodCompSc = new CompositeEmployee("Mr.V.Sarcar(HOD-CSE)", "Computer Sc.");
		CompositeEmployee principal = new CompositeEmployee("DR S.Som(Principal)", "Planning-Supervising-Managing");
		hodMaths.addEmployee(mathTeacher1);
		hodMaths.addEmployee(mathteacher2);
		
	    hodCompSc.addEmployee(cseTeacher1);
	    hodCompSc.addEmployee(cseTeacher2);
	    hodCompSc.addEmployee(cseTeacher3);
	    
	    principal.addEmployee(hodMaths);
	    principal.addEmployee(hodCompSc);
	    System.out.println("Testing the structure of a Prinncipal object");
	    principal.printStructures();
	    System.out.println("At present Principal has control over" + principal.getEmployeeCount()+"number of employees");
	    System.out.println("Testing the structure of a HOD-CSE object:");
	    hodCompSc.printStructures();
	    System.out.println("At present HOD-CSE has control over"+ hodCompSc.getEmployeeCount()+"NUMBER OF EMPLOYEE");
	    System.out.println("Testing the structure of a DOD-Maths object:");
	    hodMaths.printStructures();
	    System.out.println("At present HOD-Maths has control over"+ hodMaths.getEmployeeCount()+"number of employees");
	    
	    System.out.println("Testing the structure of a leaf node:");
	    mathTeacher1.printStructures();
	    
	    System.out.println("At present Maths Teacher -1 has control over" +mathTeacher1.getEmployeeCount()+"number of employees");
	    hodCompSc.removeEmployee(cseTeacher2);
	    
	    System.out.println("After CSE Teacher-2 resigned , the organization has folloing members:");
	    principal.printStructures();
	    System.out.println("At present Principal has control over"+principal.getEmployeeCount()+"number of employees" );
	    System.out.println("At present HOD_CSE has control over"+ hodCompSc.getEmployeeCount()+"number of employees");
	    System.out.println("At present HOD-Maths has control over"+hodMaths.getEmployeeCount()+"number of employees");
	    
	    
		
	}

}
