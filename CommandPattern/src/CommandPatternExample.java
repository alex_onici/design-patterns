
public class CommandPatternExample {
	public static void  main(String [] args) {
		Receiver intendedReceiver = new Receiver();
		MyUndoCommand undoCmd = new MyUndoCommand(intendedReceiver);
		Invoker invoker = new Invoker();
		invoker.setCommand(undoCmd);
		invoker.invokeCommand();
		
		MyRedoCommand redoCmd = new MyRedoCommand(intendedReceiver);
		invoker.setCommand(redoCmd);
		invoker.invokeCommand();
	}

}
