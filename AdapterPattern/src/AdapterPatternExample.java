
public class AdapterPatternExample {
	public static void main(String [] args) {
		CalculatorAdapter calculatorAdapter = new CalculatorAdapter();
		Triangle t = new Triangle(20, 10);
		System.out.println("Area of triangle is "+calculatorAdapter.getArea(t)+" square unit");
	}
}
