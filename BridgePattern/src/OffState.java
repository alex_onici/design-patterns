
public class OffState  implements State{

	@Override
	public void moveState() {
		System.out.println("Off state");
	}

	@Override
	public void hardPressed() {
		System.out.println("The device is Offline now.Do not press the off button again");
	}

}
