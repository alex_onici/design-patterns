
public class OnState implements State {

	@Override
	public void moveState() {
		System.out.println("On state");
	}

	@Override
	public void hardPressed() {
		System.out.println("The device is Offline now.Do not press the off button again.\n");
	}

}
