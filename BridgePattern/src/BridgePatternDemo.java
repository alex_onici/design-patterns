
public class BridgePatternDemo {
	public static void main(String [] args) {
		State presentState = new OnState();
		ElectronicGoods eItem = new Television();
		eItem.setState(presentState);
		eItem.moveToCurrentState();
		eItem.hardButtonPressed();
		
		presentState = new OffState();
		eItem = new Television();
		eItem.setState(presentState);
		eItem.moveToCurrentState();
		System.out.println("Dealing with a DVD now");
	     presentState = new OnState();
		 eItem = new DVD();
		eItem.setState(presentState);
		eItem.moveToCurrentState();
	
		
		presentState = new OffState();
		eItem = new DVD();
		eItem.setState(presentState);
		eItem.moveToCurrentState();
		
	}

}
