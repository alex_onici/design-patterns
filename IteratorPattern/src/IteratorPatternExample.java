
public class IteratorPatternExample {
	public static void main(String[]arg) {
		Subjects artsSubjects = new Arts();
		Iterator iteratorForArts = artsSubjects.createIterator();
		System.out.println("Arts subjects are as follows:");
		while(iteratorForArts.hasNext()) {
			System.out.println(iteratorForArts.next());
		}
		iteratorForArts.first();
		System.out.println("Currently poiting back to: "+iteratorForArts.currentItem());
	}
}
