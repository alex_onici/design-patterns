
public class PaintDecorator extends AbstractDecorator {

	public void makeHouse() {
		super.makeHouse();
		System.out.println("Paint decorator is in action now");
		paintTheHouse();
	}
	
	public void paintTheHouse() {
		System.out.println("Now I am painting the house");
	}
}
