
public class DecoratorPatternExample {
	public static void main(String [] args) {
		ConcreteComponent withoutDecorator = new ConcreteComponent();
		withoutDecorator.makeHouse();
		
		System.out.println("Using a floor decorator now");
		FloorDecorator floorDecorator = new FloorDecorator();
		floorDecorator.setTheComponent(withoutDecorator);
		floorDecorator.makeHouse();
		System.out.println("Using a PaintDecorator now");
		PaintDecorator paintDecorator = new PaintDecorator();
		paintDecorator.setTheComponent(floorDecorator);
		paintDecorator.makeHouse();
	}

}
