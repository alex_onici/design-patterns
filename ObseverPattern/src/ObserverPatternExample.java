
public class ObserverPatternExample {
	public static void main(String[] args) {
		Observer myObserver1 = new ObserverType1("Roy");
		Observer myObserver2 = new ObserverType2("Kevin");
		Observer myObserver3 = new ObserverType1("Bose");
		Subject subject = new Subject();
		subject.register(myObserver1);
		subject.register(myObserver2);
		subject.register(myObserver3);
		subject.setFlag(5);
		subject.unregister(myObserver1);
		subject.setFlag(50);
		subject.register(myObserver1);
		subject.setFlag(100);
		
	}

}
