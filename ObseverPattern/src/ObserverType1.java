
public class ObserverType1 implements Observer {

	String nameOfObserver;
	public ObserverType1(String name) {
		this.nameOfObserver = name;
	}
	
	@Override
	public void update(int updateValue) {
		System.out.println(nameOfObserver+"has received an alert:Updated myValue in Subjectis: "+updateValue);
	}

}
