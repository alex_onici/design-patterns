
public class Unknow extends Employee {

	public Unknow(Mediator mediator, String name) {
		super(mediator);
		this.name = name;
	}
	@Override
	public String employeeType() {
		return "Outsider";
	}

}
