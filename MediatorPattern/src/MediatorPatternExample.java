
public class MediatorPatternExample {
	public static void main(String[] args) throws InterruptedException {
		ConcreteMediator mediator = new ConcreteMediator();
		JuniorEmployee amit = new JuniorEmployee(mediator, "Amit");
		JuniorEmployee sohel = new JuniorEmployee(mediator, "Sohel");
		SeniorEmployee reghu = new SeniorEmployee(mediator, "Raghu");
		mediator.register(amit);
		mediator.register(sohel);
		mediator.register(reghu);
		mediator.displayRegisteredEmployees();
		System.out.println("Communications starts among participants");
		amit.sendMessage("Hi Sohel,can we discuss the mediator pattern?");
		sohel.sendMessage("Hi Amit, yup,we can discuss now");
		reghu.sendMessage("Please get back to work quickly");
		Unknow unknow = new Unknow(mediator, "Jack");
		unknow.sendMessage("hello guys");
	}
}
